# Contributor: Oleg Titov <oleg.titov@gmail.com>
# Maintainer: Oleg Titov <oleg.titov@gmail.com>
pkgname=xmrig
pkgver=6.5.3
pkgrel=0
pkgdesc="XMRig is a high performance Monero (XMR) miner"
url="https://xmrig.com/"
arch="aarch64 x86 x86_64" # officially supported by upstream
license="GPL-3.0-or-later"
options="!check" # No test suite from upstream
makedepends="cmake libmicrohttpd-dev libuv-dev openssl-dev hwloc-dev"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/xmrig/xmrig/archive/v$pkgver.tar.gz"

case "$CARCH" in
	aarch64*) CMAKE_CROSSOPTS="-DARM_TARGET=8"; makedepends="$makedepends linux-headers" ;;
esac

build() {
	cmake -B build $CMAKE_CROSSOPTS -DCMAKE_BUILD_TYPE=None \
		-DWITH_CUDA=OFF \
		-DWITH_OPENCL=OFF

	make -C build
}

package() {
	install -Dm 755 build/xmrig $pkgdir/usr/bin/xmrig

	install -Dm 644 -t "$pkgdir"/usr/share/doc/$pkgname/ README.md
}

sha512sums="fec8ddbdaba39d3999feff84f509ae0f6ffd842bf330f8c79bcbc624d6a81468b93b8f99dd34f1a3092f6f45300e585d289a821d35fa81cbb0cd9ef192a7fc9e  xmrig-6.5.3.tar.gz"
